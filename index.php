<?php
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
    //Импорт автолоадера
    require 'App\core\Autoloader.php';
    require 'App\core\Kernel.php';
    use Core\App\Kernel;
    use Core\App\App;

    //Подключение автолоадера до вызова Kernel
    $dir = 'App/core/autoload';
    $filesArr = scandir($dir);

    autoload($filesArr);

    $kernel = new Kernel(new App());
    print_r($kernel);

