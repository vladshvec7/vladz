<?php

namespace App\core\autoload;

class DB
{
    protected string $host;
    protected string $username;
    protected string $password;
    protected string $dbName;
    protected $dbConnectCash;
    public function __construct($host,$username,$password,$dbName)
    {
       $this->host = $host;
       $this->username = $username;
       $this->password = $password;
       $this->dbName = $dbName;
    }

    public function connect()
    {
        if(empty($this->dbConnectCash)){

        }
        return mysqli_connect($this->host,$this->username,$this->password,$this->dbName);
    }
}