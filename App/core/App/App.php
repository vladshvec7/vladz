<?php

namespace Core\App;

class App implements HaveContainerContract, AppContract
{
    private static $instance;
    private function __constructor(){

    }

    public function getInstance()
    {
        if(!self::$instance){
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function start()
    {

    }

    private function setContainer()
    {

    }

    private function getContainer()
    {

    }

    private function request()
    {

    }

    private function response()
    {

    }
}