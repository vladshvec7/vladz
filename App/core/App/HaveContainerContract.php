<?php


namespace Core\App;


interface HaveContainerContract
{
    public function getInstance();
}