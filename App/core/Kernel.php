<?php

namespace Core\App;

require "App/conf/conf.php";
use Core\App\AppContract;
use http\Client\Request;
use http\Client\Response;


class Kernel
{
    private AppContract $app;
    private Response $response;

    public function __construct(AppContract $app)
    {
        $this->app = $app;
    }

    public function handle(Request $request)
    {

    }

    public function send() : Response
    {
      return $this->response;
    }
}